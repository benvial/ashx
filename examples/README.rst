.. _examples-index:

Examples
========

Typical examples of application of the package are presented here. 
You can run them live in your browser on `mybinder.org <https://mybinder.org/v2/gl/benvial%2Fashx/doc?urlpath=lab/tree/notebooks/>`_.
