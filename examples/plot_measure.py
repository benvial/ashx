#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: benvial
# This file is part of ashx
# License: GPLv3
# See the documentation at  gitlab.com/benvial/ashx

# Note: to cite a paper
# Checking results from :cite:p:`bibtextag`.

"""
Measure
========

Defining integration measures.
"""

import os

import numpy as np
from dolfinx.fem import Constant

import ashx

######################################################################
# Read the mesh file

data_dir = "data"

mesh, ct, ft, msh = ashx.gmsh2dolfinx(
    0,
    mshfile=os.path.join(data_dir, "tri.msh"),
    dim=2,
)
######################################################################
# The subdomains are a square of size 0.3 (incl, subdomain 2)
# inside another of size 1 (cell, subdomain 1)

subdomains_dict = dict(cell=1, incl=2)

######################################################################
# Define the measure
dx = ashx.Measure(
    "dx",
    domain=mesh,
    subdomain_data=ct,
    subdomain_dict=subdomains_dict,
)
Id = Constant(mesh, 1.0)


Vcell = ashx.integrate(Id, dx("cell"))
Vincl = ashx.integrate(Id, dx("incl"))

Vtot = ashx.integrate(Id, dx(["cell", "incl"]))
print(Vcell, Vincl, Vincl + Vcell)
print(1 - 0.3**2, 0.3**2, 1)
assert np.allclose(ashx.integrate(Id, dx), Vtot)
