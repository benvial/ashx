#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: benvial
# This file is part of ashx
# License: GPLv3
# See the documentation at  gitlab.com/benvial/ashx

# Note: to cite a paper
# Checking results from :cite:p:`bibtextag`.

"""
Geometry
========

Defining a geometry.
"""


import os

import pyvista
from dolfinx.fem import Function

import ashx

######################################################################
# Create a Geometry opbject from a mesh file

data_dir = "data"
geom = ashx.BaseGeometry(
    mshfile=os.path.join(data_dir, "tri.msh"),
    dim=2,
)

######################################################################
# The subdomains are a square of size 0.3 (incl, subdomain 2)
# inside another of size 1 (cell, subdomain 1)
# Those were defined with ``gmsh`` using physical groups

print(geom.physical)
print(geom.subdomains)
print(geom.boundaries)


######################################################################
# ``geom`` has an attribute ``Vmat`` which is a DG function space do define
# piecewise functions, eg material properties

f = Function(geom.Vmat)


def pvplot(grid, value, name):
    p = pyvista.Plotter(window_size=[800, 800])
    grid.cell_data[name] = value
    grid.set_active_scalars(name)
    p.add_mesh(grid, show_edges=True)
    p.view_xy()
    if pyvista.OFF_SCREEN:
        figure = p.screenshot("subdomains_structured.png")
    p.show()


def plot_subdomains(geom):
    marker = geom.data["cells"].values
    mesh = geom.mesh
    grid = ashx.get_grid(mesh)
    pvplot(grid, marker, "Markers")


def plot_piecewise(fun):
    values = fun.x.array[:]
    mesh = fun.function_space.mesh
    grid = ashx.get_grid(mesh)
    pvplot(grid, values, fun.name)


values = dict(inclusion=7, cell=90)
fun = geom.piecewise(values)
fun.name = "Test"
plot_subdomains(geom)
plot_piecewise(fun)
