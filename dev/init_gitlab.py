#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: benvial
# This file is part of ashx
# License: GPLv3
# See the documentation at  gitlab.com/benvial/ashx

import configparser
import os

import gitlab

gl = gitlab.Gitlab.from_config(None, [os.path.expanduser("~/.python-gitlab.cfg")])
name = "benvial/ashx"

try:
    project = gl.projects.create({"name": "ashx", "visibility": "public"})
except:
    project = gl.projects.get(name)

with open("justfile", "r+") as f:
    filedata = f.read()
    filedata = filedata.replace("GITLAB_PROJECT_ID_PLACEHOLDER", f"{project.id}")
    f.seek(0)
    f.write(filedata)
    f.truncate()

# project.delete()

try:
    with open(os.path.expanduser("~/.ssh/id_rsa"), "r+") as f:
        SSH_PRIVATE_KEY = f.read()

    var = project.variables.create(
        {"key": "SSH_PRIVATE_KEY", "value": f"{SSH_PRIVATE_KEY}"}
    )
    # var = project.variables.get('SSH_PRIVATE_KEY')
    var.protected = True
    var.save()
except:
    print("Could not create SSH_PRIVATE_KEY variable.")


config = configparser.ConfigParser()

try:
    config.read(os.path.expanduser("~/.pypirc"))

    var = project.variables.create(
        {"key": "TWINE_USERNAME", "value": f"{config['pypi']['username']}"}
    )
    var.protected = True
    var.save()
    var = project.variables.create(
        {"key": "TWINE_PASSWORD", "value": f"{config['pypi']['password']}"}
    )
    var.masked = True
    var.protected = True
    var.save()
except:
    print("Could not create twine login variables.")

try:
    config.read(os.path.expanduser("~/.zenodorc"))

    var = project.variables.create(
        {"key": "zenodo_token", "value": f"{config['zenodo']['token']}"}
    )
    var.protected = True
    var.masked = True
    var.save()
except:
    print("Could not create twine zenodo_token variable.")
