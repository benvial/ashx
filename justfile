
PROJECT_NAME := "ashx"
USER_NAME := "benvial"


BRANCH := "$(git branch --show-current)"

PROJECT_DIR := "$(realpath $PWD)"

VERSION := """$(python3 -c "import toml; print(toml.load('pyproject.toml')['project']['version'])")"""



GITLAB_PROJECT_ID := "52915095"

# Echo information
info:
    @echo {{PROJECT_NAME}} version {{VERSION}}, on branch {{BRANCH}}
    @echo directory {{PROJECT_DIR}}


# List recipes
list:
    just -l

# Make conda environment
conda-env:
    mamba env create -f environment.yml

# Install the python package locally in editable mode
install:
    pip install -e .

# Install development dependencies
dev:
    pip install -e .[dev]

# Install test dependencies
test-req:
    pip install -e .[test]

# Install documentation dependencies
doc-req:
    pip install -e .[doc]

# Install all dependencies
req: install doc-req test-req dev


# Build html documentation (only updated examples)
doc: 
    cd doc && make -s html
    just postpro-doc

# Postprocess html documentation 
postpro-doc: 
    cd doc && make -s postpro

# Build html documentation (live reload)  
livedoc:   
    sphinx-autobuild -a doc doc/_build/html --watch examples/ --watch doc/_templates/ \
    --watch doc/_static/  --port=8001 --open-browser --delay 1 \
    --re-ignore 'doc/examples/*'

# Show html documentation in the default browser
show:
    cd doc && make -s show

# Show code coverage report
show-cov:
    python -m webbrowser htmlcov/index.html

# Cleanup
clean:
    cd doc && make -s clean
    rm -rf build dist


# Generate documentation api
api:
    rm -rf doc/api/         
    sphinx-apidoc -o doc/api {{PROJECT_NAME}}/ -f --implicit-namespaces


# Lint using flake8
lint:
	flake8 --exit-zero --ignore=E501,W503 {{PROJECT_NAME}} test/*.py examples/

# Check for duplicated code
dup:
	pylint --exit-zero -f colorized --disable=all --enable=similarities {{PROJECT_NAME}}

# Reformat code
style:
	@isort .
	@black .

# Update header text
header:
	@cd dev && python update_header.py

# Run tests
test:
    @export MPLBACKEND=agg  && pytest ./test --cov={{PROJECT_NAME}} --cov-report term  --cov-report html --cov-report xml --durations=1
#  --disable-warnings

# Push to gitlab
gl:
    @git add -A
    @read -p "Enter commit message: " MSG; \
    git commit -a -m "$MSG"
    @git push origin {{BRANCH}}


# Show gitlab repository
repo:
	xdg-open https://gitlab.com/{{USER_NAME}}/{{PROJECT_NAME}}


# Clean, reformat and push to gitlab
save: style gl


# Check we are on the main branch
checkmain:
	@if [ "{{ BRANCH }}" != "main" ]; then exit 1; fi

# Tag and push tags
tag: clean style checkmain
	@echo "Version v{{VERSION}}"
	# @git add -A
	# git commit -a -m "Publish v{{VERSION}}"
	# @git push origin {{BRANCH}}
	@git tag v{{VERSION}} || echo Ignoring tag since it already exists
	@git push --tags || echo Ignoring tag since it already exists on the remote

# Create a release
release: checkmain
	@gitlab project-release create --project-id {{GITLAB_PROJECT_ID}} \
	--name "version {{VERSION}}" --tag-name "v{{VERSION}}" --description "Released version {{VERSION}}"


# Create python package
package: checkmain
	@rm -f dist/*
	@python3 -m build --sdist --wheel .

# Upload to pypi
pypi: package
	@twine upload dist/*


# Publish release on pypi
publish: header tag release pypi

# Init gitlab
init-gitlab: 
	python dev/init_gitlab.py

init-git: 
    git init --initial-branch=main
    git remote add origin git@gitlab.com:{{USER_NAME}}/{{PROJECT_NAME}}.git
    git add .
    git commit -m "Initial commit [ci skip]"
    git push --set-upstream origin main


