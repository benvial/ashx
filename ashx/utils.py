#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: benvial
# This file is part of ashx
# License: GPLv3
# See the documentation at  gitlab.com/benvial/ashx


"""Utility functions

This module provides various utility functions, including:

- projection of a function onto a function space
- conversion of a function to an array
- integration of a function over a domain
- creation of a grid for plotting
"""


__all__ = [
    "project",
    "array2function",
    "function2array",
    "integrate",
    "get_grid",
    "Constant",
]


import numpy as np
import pyvista
import ufl
from dolfinx import default_scalar_type
from dolfinx import plot as dfplot
from dolfinx.fem import Constant as _Constant
from dolfinx.fem import Function, assemble_scalar, form
from dolfinx.fem.petsc import LinearProblem
from ufl import TestFunction, TrialFunction, dx, inner


def Constant(domain, c):
    return _Constant(domain, default_scalar_type(c))


def integrate(fun, dx=None, domain=None):
    """
    Integrate a given function over a domain.

    Parameters
    ----------
    fun : Function or Expression
        The function to integrate
    dx : ufl.Measure, optional
        The measure to integrate over, by default None (i.e. dx)
    domain : dolfinx.mesh.Mesh, optional
        The mesh over which to integrate, by default None (i.e. the mesh
        associated with the function)

    Returns
    -------
    float or complex
        The integrated value

    Notes
    -----
    The integration is performed by assembling the form using
    `assemble_scalar`.
    """
    dx = dx or ufl.dx
    if domain is not None:
        dx = dx(domain)
    return assemble_scalar(form(fun * dx))


def array2function(array, function=None, space=None):
    """
    Converts a numpy array to a dolfinx Function.

    Parameters
    ----------
    array : numpy.array
        The array to convert
    function : Function, optional
        A dolfinx Function to overwrite with the array, by default None
    space : FunctionSpace, optional
        The function space to create a new Function with, by default None

    Returns
    -------
    Function
        The converted Function

    Notes
    -----
    If function is None, a new dolfinx Function is created using the given space.
    Otherwise, the given function is overwritten with the array.
    """
    if function is None and space is None:
        raise ValueError("You must specify either function or space.")
    elif function is None:
        # Create a new Function
        function = Function(space)
    else:
        # Overwrite the given Function
        pass
    function.x.array[:] = array
    return function


def function2array(function):
    """Converts a dolfinx Function to a numpy array.

    Parameters
    ----------
    function : Function
        The function to extract the array from

    Returns
    -------
    array
        The array with function values
    """
    return function.x.array


def project(function, space, bcs=[]):
    """
    Project a function into a function space.

    Parameters
    ----------
    function : Function
        The function to project
    space : functionspace
        The function space to project into
    bcs : list, optional
        Boundary conditions, by default []

    Returns
    -------
    Function
        The projected function

    Notes
    -----
    The function is projected using a Galerkin projection, i.e.
    the inner product of the function with the test functions is computed
    and the resulting linear system is solved using a direct solver.
    """
    # Define the trial and test functions
    u, v = TrialFunction(space), TestFunction(space)

    # Define the bilinear form
    A = inner(u, v) * dx

    # Define the linear form
    b = inner(function, v) * dx

    # Define the linear problem
    problem = LinearProblem(
        A,
        b,
        bcs=bcs,
        petsc_options={"ksp_type": "preonly", "pc_type": "lu"},
    )

    # Solve the linear problem
    return problem.solve()


def get_grid(mesh):
    """
    Filter out ghosted cells from a dolfinx mesh and return a pyvista UnstructuredGrid.

    Parameters
    ----------
    mesh : dolfinx.mesh.Mesh
        The input mesh

    Returns
    -------
    pyvista.UnstructuredGrid
        The filtered mesh
    """
    # Get the cell topology
    cell_dim = mesh.topology.dim
    num_cells_local = mesh.topology.index_map(cell_dim).size_local

    # Create a connectivity array
    mesh.topology.create_connectivity(cell_dim, cell_dim)

    # Create the VTK mesh
    topology, cell_types, points = dfplot.vtk_mesh(
        mesh, cell_dim, np.arange(num_cells_local, dtype=np.int32)
    )

    # Create the pyvista grid
    grid = pyvista.UnstructuredGrid(topology, cell_types, points)

    return grid
