#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: benvial
# This file is part of ashx
# License: GPLv3
# See the documentation at  gitlab.com/benvial/ashx

"""Mesh functions and classes.

This module provides functions and classes to handle the creation and managment
of meshes. In particular, it provides a function to convert a Gmsh mesh to a
Dolfinx mesh.
"""

__all__ = ["create_mesh", "gmsh2dolfinx"]

import os
import tempfile

import meshio
from dolfinx.io import XDMFFile, gmshio
from mpi4py import MPI


def create_mesh(mesh, cell_type, prune_z=False):
    """
    Create a dolfinx mesh from a meshio mesh.

    Parameters
    ----------
    mesh : meshio.Mesh
        The input mesh
    cell_type : str
        The cell type to extract from the mesh
    prune_z : bool, optional
        If True, the z coordinate of the vertices will be removed

    Returns
    -------
    dolfinx.mesh.Mesh
        The output mesh
    """
    cells = mesh.get_cells_type(cell_type)
    physical_data = mesh.cell_data_dict["gmsh:physical"].keys()
    if cell_type in physical_data:
        cell_data = mesh.get_cell_data("gmsh:physical", cell_type)
    else:
        cell_data = []
    points = mesh.points[:, :2] if prune_z else mesh.points
    return meshio.Mesh(
        points=points, cells={cell_type: cells}, cell_data={"name_to_read": [cell_data]}
    )


# TODO: deal with physical points in 2D and physiscal points/lines in 3D


def gmsh2dolfinx(
    proc,
    mshfile=None,
    cellxdmf=None,
    facetxdmf=None,
    data_dir=None,
    dim=3,
    quad=False,
    rank=0,
):
    """
    Convert a Gmsh file to a DOLFINx mesh, and save the mesh and its facets to
    two XDMF files.

    Parameters
    ----------
    proc : int
        The MPI process number
    mshfile : str, optional
        The path to the Gmsh file, by default None (i.e. "mesh.msh")
    cellxdmf : str, optional
        The path to the XDMF file for the mesh, by default None (i.e. "cells.xdmf")
    facetxdmf : str, optional
        The path to the XDMF file for the facets, by default None (i.e. "facets.xdmf")
    data_dir : str, optional
        The directory where the XDMF files are stored, by default None (i.e. a
        temporary directory is created)
    dim : int, optional
        The dimension of the mesh, by default 3
    quad : bool, optional
        If True, the mesh is a quadrilateral mesh, by default False

    Returns
    -------
    mesh : dolfinx.mesh.Mesh
        The DOLFINx mesh
    ct : dolfinx.mesh.MeshTags
        The cell markers
    ft : dolfinx.mesh.MeshTags
        The facet markers
    msh : meshio.Mesh
        The mesh in the meshio format
    """
    if dim not in [2, 3]:
        raise ValueError
    if dim == 3:
        cell = "hexahedron" if quad else "tetra"
        facet = "quad" if quad else "triangle"
    else:
        cell = "quad" if quad else "triangle"
        facet = "line"

    data_dir = data_dir or tempfile.mkdtemp()
    mshfile = mshfile or "mesh.msh"
    cellxdmf = cellxdmf or "cells.xdmf"
    facetxdmf = facetxdmf or "facets.xdmf"
    # mshfile = os.path.join(data_dir, mshfile)
    cellxdmf = os.path.join(data_dir, cellxdmf)
    facetxdmf = os.path.join(data_dir, facetxdmf)

    mesh, cell_markers, facet_markers = gmshio.read_from_msh(
        mshfile, MPI.COMM_WORLD, rank, gdim=dim
    )
    prune_z = dim != 3

    if proc == 0:
        # Read in mesh
        msh = meshio.read(mshfile)
        # Create and save one file for the mesh, and one file for the facets
        cell_mesh = create_mesh(msh, cell, prune_z=prune_z)
        facet_mesh = create_mesh(msh, facet, prune_z=prune_z)
        meshio.write(cellxdmf, cell_mesh)
        meshio.write(facetxdmf, facet_mesh)

    with XDMFFile(MPI.COMM_WORLD, cellxdmf, "r") as xdmf:
        mesh = xdmf.read_mesh(name="Grid")
        ct = xdmf.read_meshtags(mesh, name="Grid")
    mesh.topology.create_connectivity(mesh.topology.dim, mesh.topology.dim - 1)
    with XDMFFile(MPI.COMM_WORLD, facetxdmf, "r") as xdmf:
        ft = xdmf.read_meshtags(mesh, name="Grid")
    return mesh, ct, ft, msh
