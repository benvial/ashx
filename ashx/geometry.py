#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: benvial
# This file is part of ashx
# License: GPLv3
# See the documentation at  gitlab.com/benvial/ashx

"""Geometry definition

This module contains the definition of the `BaseGeometry` class, which provides a
set of methods and properties to handle the geometry of a problem. In
particular, it provides a way to define the physical regions of the problem,
their boundaries, and the corresponding mesh. The geometry is defined using the
Gmsh format.
"""

__all__ = ["BaseGeometry", "Domain"]


import tempfile

import numpy as np
from dolfinx.fem import Constant, Function, form, functionspace
from dolfinx.fem.petsc import assemble_vector
from dolfinx.mesh import meshtags
from petsc4py.PETSc import ScalarType
from ufl import TestFunction, inner

from .measure import Measure
from .mesh import gmsh2dolfinx
from .utils import integrate


class BaseGeometry:
    """
    Base geometry object.

    Parameters
    ----------
    dim : int
        Dimension of the mesh
    mshfile : str
        The path to the Gmsh file containing the mesh
    data_dir : str, optional
        Directory where the mesh data is stored, by default None (i.e. a
        temporary directory is created)
    proc : int, optional
        The MPI process number, by default 0
    """

    def __init__(self, dim, mshfile, data_dir=None, proc=0):
        self.data_dir = data_dir or tempfile.mkdtemp()
        self.dim = dim
        self.proc = proc

        self.mesh, ct, ft, self.mshio_mesh = gmsh2dolfinx(
            proc, mshfile, data_dir=self.data_dir, dim=dim
        )

        self.data = dict(cells=ct, facets=ft)
        self.physical = self.mshio_mesh.field_data
        self.cells = {}
        self.facets = {}
        self.subdomain_map = {}
        self.facets_map = {}
        self.meshtags = {}
        for dom, dimtag in self.physical.items():
            if dimtag[1] == dim:
                self.cells[dom] = self.data["cells"].find(dimtag[0])
                self.subdomain_map[dom] = dimtag[0]
                arg_sort = np.argsort(self.cells[dom])
                self.meshtags[dom] = meshtags(
                    self.mesh,
                    dim,
                    self.cells[dom][arg_sort],
                    np.full(len(self.cells[dom]), dimtag[0], dtype=np.int32),
                )
            if dimtag[1] == dim - 1:
                self.facets[dom] = self.data["facets"].find(dimtag[0])
                self.facets_map[dom] = dimtag[0]
                arg_sort = np.argsort(self.facets[dom])
                self.meshtags[dom] = meshtags(
                    self.mesh,
                    dim,
                    self.facets[dom][arg_sort],
                    np.full(len(self.facets[dom]), dimtag[0], dtype=np.int32),
                )

        self.subdomains = list(self.cells.keys())
        self.boundaries = list(self.facets.keys())
        self.num_sub_cells = {}
        for subdomain in self.subdomains:
            self.num_sub_cells[subdomain] = len(self.cells[subdomain])
        self.num_cells = sum(self.num_sub_cells.values())
        self.num_sub_facets = {}
        for boundary in self.boundaries:
            self.num_sub_facets[subdomain] = len(self.facets[boundary])
        self.num_facets = sum(self.num_sub_facets.values())
        self.Vmat = functionspace(self.mesh, ("DG", 0))

        self.dx = Measure(
            "dx",
            domain=None,
            subdomain_id="everywhere",
            metadata=None,
            subdomain_data=self.data["cells"],
            subdomain_dict=self.subdomain_map,
        )
        self.dS = Measure(
            "dS",
            domain=None,
            subdomain_id="everywhere",
            metadata=None,
            subdomain_data=self.data["facets"],
            subdomain_dict=self.facets_map,
        )
        self.ds = Measure(
            "ds",
            domain=None,
            subdomain_id="everywhere",
            metadata=None,
            subdomain_data=self.data["facets"],
            subdomain_dict=self.facets_map,
        )

    def integrate(self, f, subdomain="everywhere", metadata={}):
        """
        Integrate a given function over the entire domain or a subset of it.

        Parameters
        ----------
        f : Function or Expression
            The function to integrate
        subdomain : str, optional
            The physical subdomain to integrate over, by default "everywhere" (i.e. the entire domain)
        metadata : dict, optional
            Additional parameters to pass to the :class:`Measure` constructor, by default None

        Returns
        -------
        float or complex
            The integrated value
        """
        return integrate(f, self.dx(metadata=metadata, subdomain_id=subdomain))

    def constant(self, c):
        """
        Create a constant function over the entire domain.

        Parameters
        ----------
        c : float or complex
            The value of the constant function

        Returns
        -------
        Function
            The constant function

        Notes
        -----
        The constant function is a dolfinx Function with a scalar value.
        """
        return Constant(self.mesh, ScalarType(c))

    def get_volume(self, physical_id="everywhere"):
        """
        Return the volume of a given physical subdomain.

        Parameters
        ----------
        physical_id : str, optional
            The physical subdomain to compute the volume of, by default "everywhere" (i.e. the entire domain)

        Returns
        -------
        float
            The volume of the subdomain
        """
        return self.integrate(self.constant(1), physical_id)

    @property
    def cell_vol(self):
        """
        Returns the volume of each cell in the mesh.

        Returns
        -------
        array
            The volume of each cell in the mesh
        """
        q_degree = 0
        vol = Function(self.Vmat)
        assemble_vector(
            vol.x.petsc_vec,
            form(
                inner(ScalarType(1), TestFunction(self.Vmat))
                * self.dx(metadata={"quadrature_degree": q_degree})
            ),
        )
        vol.x.scatter_forward()
        return vol.x.array.real

    def piecewise(self, subdomain_dict, pw_fun=None, default_value=0):
        """
        Define a piecewise function defined on each subdomain.

        Parameters
        ----------
        subdomain_dict : dict
            A dictionary mapping physical subdomains to values. The values can be either
            a scalar or a callable.
        pw_fun : Function, optional
            The function to overwrite with the piecewise constant values, by default None
        default_value : float or complex, optional
            The value to assign to cells that are not in any of the given subdomains, by default 0

        Returns
        -------
        Function
            The piecewise function
        """
        pw_fun = pw_fun or Function(self.Vmat)
        pw_fun.x.array[:] = ScalarType(default_value)
        for subdomain, value in subdomain_dict.items():
            # Get the cells for the current subdomain
            cells = self.cells[subdomain]
            if callable(value):
                # Evaluate the callable on the cells
                _pw_fun = Function(self.Vmat)
                _pw_fun.interpolate(value)
                # Copy the values from the temporary function to the output function
                pw_fun.x.array[cells] = ScalarType(_pw_fun.x.array[cells])
                pw_fun.x.scatter_forward()
            else:
                # Assign the value to the cells
                pw_fun.x.array[cells] = ScalarType(value)
        return pw_fun


class Domain(BaseGeometry):
    def __init__(self, geometry, data_dir=None, proc=0):
        """
        Initialize a Domain object from a geometry.

        Parameters
        ----------
        geometry : geolia.Geometry
            The geometry of the domain.
        data_dir : str, optional
            The directory where the mesh file is stored, by default None
        proc : int, optional
            The processor number, by default 0
        """
        super().__init__(geometry.dim, geometry.msh_file, data_dir, proc)
