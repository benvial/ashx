#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: benvial
# This file is part of ashx
# License: GPLv3
# See the documentation at  gitlab.com/benvial/ashx

"""
Custom integration measures
==========================

This module provides a custom implementation of the
:class:`ufl.Measure` class, which is used to represent
integration measures in UFL.

The main difference between this implementation and the
original one is that it allows for a more flexible way
of specifying the subdomain id and the metadata associated
with the measure.

The :class:`Measure` class is a subclass of :class:`ufl.Measure`
and provides the same interface, with the addition of
an extra argument ``subdomain_dict`` which can be used
to specify the mapping between physical domain strings
and integer ids.

The :class:`Measure` class is used internally by the
:func:`integrate` function to create a :class:`Measure`
object from a given function, subdomain id and metadata.

The :func:`integrate` function is a wrapper around
:func:`dolfinx.fem.assemble_scalar` which takes care
of creating the :class:`Measure` object and assembling
the form.
"""

__all__ = ["Measure"]

import ufl


class Measure(ufl.Measure):
    """
    Measure with extra functionalities.

    Parameters
    ----------
    integral_type : str
        The type of integral
    domain : dolfinx.mesh.Mesh, optional
        The domain over which to integrate, by default None
    subdomain_id : int, str, or list of int, optional
        The subdomain(s) to integrate over, by default "everywhere" (i.e. the entire domain)
    metadata : dict, optional
        Additional parameters to pass to the :class:`Measure` constructor, by default None
    subdomain_data : dolfinx.mesh.MeshTags, optional
        Object representing data to interpret subdomain_id with, by default None
    subdomain_dict : dict, optional
        Mapping from physical domain strings to integer ids, by default None
    """

    def __init__(
        self,
        integral_type,
        domain=None,
        subdomain_id="everywhere",
        metadata=None,
        subdomain_data=None,
        subdomain_dict=None,
    ):

        self.subdomain_dict = subdomain_dict
        if (
            self.subdomain_dict
            and isinstance(subdomain_id, str)
            and subdomain_id != "everywhere"
        ):
            subdomain_id = self.subdomain_dict[subdomain_id]
        super().__init__(
            integral_type,
            domain=domain,
            subdomain_id=subdomain_id,
            metadata=metadata,
            subdomain_data=subdomain_data,
        )

    def __call_single__(self, subdomain_id=None, **kwargs):
        """
        Evaluate the measure on a single subdomain.

        Parameters
        ----------
        subdomain_id : int, str, or None
            The subdomain to evaluate the measure on. If None, the measure is
            evaluated on the entire domain. If a string, the subdomain id is
            looked up in the :attr:`subdomain_dict` attribute.

        Returns
        -------
        value : float
            The value of the measure on the given subdomain
        """
        if (
            self.subdomain_dict
            and isinstance(subdomain_id, str)
            and subdomain_id != "everywhere"
        ):
            subdomain_id = self.subdomain_dict[subdomain_id]
        return super().__call__(subdomain_id=subdomain_id, **kwargs)

    def __call__(self, subdomain_id=None, **kwargs):
        """
        Evaluate the measure on a single or multiple subdomains.

        Parameters
        ----------
        subdomain_id : int, str, or None
            The subdomain to evaluate the measure on. If None, the measure is
            evaluated on the entire domain. If a string, the subdomain id is
            looked up in the :attr:`subdomain_dict` attribute. If a list, the
            measure is evaluated on each subdomain in the list and the results
            are summed.

        Returns
        -------
        value : float
            The value of the measure on the given subdomain(s)
        """
        subdomain_id = None if subdomain_id == [] else subdomain_id
        if not isinstance(subdomain_id, list):
            return self.__call_single__(subdomain_id=subdomain_id, **kwargs)
        for i, sid in enumerate(subdomain_id):
            if i == 0:
                out = self.__call_single__(subdomain_id=sid, **kwargs)
            else:
                out += self.__call_single__(subdomain_id=sid, **kwargs)
        return out
