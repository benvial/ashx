
<!-- start badges -->

<a class="reference external image-reference" href="https://gitlab.com/benvial/ashx/-/releases" target="_blank"><img alt="Release" src="https://img.shields.io/endpoint?url=https://gitlab.com/benvial/ashx/-/jobs/artifacts/main/raw/logobadge.json?job=badge&labelColor=c9c9c9"></a> 
<a class="reference external image-reference" href="https://gitlab.com/benvial/ashx/commits/main" target="_blank"><img alt="Release" src="https://img.shields.io/gitlab/pipeline/benvial/ashx/main?logo=gitlab&labelColor=dedede&style=for-the-badge"></a> 
<a class="reference external image-reference" href="https://benvial.gitlab.io/ashx" target="_blank"><img alt="License" src="https://img.shields.io/badge/documentation-website-dedede.svg?logo=readthedocs&logoColor=e9d672&style=for-the-badge"></a>
<a class="reference external image-reference" href="https://gitlab.com/benvial/ashx/commits/main" target="_blank"><img alt="Release" src="https://img.shields.io/gitlab/coverage/benvial/ashx/main?logo=python&logoColor=e9d672&style=for-the-badge"></a>
<a class="reference external image-reference" href="https://black.readthedocs.io/en/stable/" target="_blank"><img alt="Release" src="https://img.shields.io/badge/code%20style-black-dedede.svg?logo=python&logoColor=e9d672&style=for-the-badge"></a>
<a class="reference external image-reference" href="https://gitlab.com/benvial/ashx/-/blob/main/LICENSE.txt" target="_blank"><img alt="License" src="https://img.shields.io/badge/license-GPLv3-blue?color=aec2ff&logo=open-access&logoColor=aec2ff&style=for-the-badge"></a>

<!-- end badges -->

# ASHX


<!-- start elevator-pitch -->

**FEniCSx tools**

ASHX is a collection of tools and helpers for using [FEniCSx](https://fenicsproject.org) for finite element simulations. The package is built on top of Python and is designed for users who want to use FEniCSx without having to worry about the underlying C++ implementation.

The package provides a simple, Pythonic interface for creating and manipulating meshes, defining and solving partial differential equations, and visualizing the results of simulations. It also includes a number of helpful utilities for working with FEniCSx, such as functions for converting meshes between different formats and classes for defining and manipulating geometry.

The main features of ASHX include:


- **Convert gmsh meshes to dolfinx**
- **Base class for geometry definition**
- **Helpers for integration and measure**


<!-- end elevator-pitch -->


# Documentation

See the website with API reference and some examples at [benvial.gitlab.io/ashx](https://benvial.gitlab.io/ashx).



<!-- start installation -->

# Installation


## From Pypi

Simply run

```bash 
pip install ashx
```

<!-- ## From conda/mamba


```bash 
mamba install -c conda-forge ashx
``` -->

## From source

Clone the repository

```bash 
git clone https://gitlab.com/benvial/ashx.git
```

Install the package locally

```bash 
cd ashx
pip install -e .
```


## From gitlab

```bash 
pip install -e git+https://gitlab.com/benvial/ashx.git#egg=ashx
```


<!-- end installation -->