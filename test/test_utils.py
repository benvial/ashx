#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: benvial
# This file is part of ashx
# License: GPLv3
# See the documentation at  gitlab.com/benvial/ashx


import os

import numpy as np
import pytest
from dolfinx.fem import Function, functionspace

import ashx
from ashx import array2function, function2array

data_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data")

mesh, ct, ft, msh = ashx.gmsh2dolfinx(
    0,
    mshfile=os.path.join(data_dir, "tri.msh"),
    dim=2,
)
V = functionspace(mesh, ("Lagrange", 2))


def test_array2function():
    u = Function(V)
    shape = u.x.array.size
    array = np.random.rand(shape)
    with pytest.raises(ValueError):
        f = array2function(array)
    f = array2function(array, u)
    f = array2function(array, space=V)


def test_project():
    u = Function(V)
    u.x.petsc_vec.array[:] = 1
    Vproj = functionspace(mesh, ("DG", 2))

    uproj = ashx.project(u, Vproj)
    tests = []
    f = Function(Vproj)
    for i in range(2048):
        test = array2function(function2array(uproj), function=f)
        tests.append(test)
    assert np.allclose(uproj.x.petsc_vec.array, 1)
    assert np.allclose(
        array2function(function2array(uproj), space=Vproj).x.petsc_vec.array, 1
    )


def test_grid():
    ashx.get_grid(mesh)
