#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: benvial
# This file is part of ashx
# License: GPLv3
# See the documentation at  gitlab.com/benvial/ashx

import geolia as gl

import ashx

a = 1
pmesh = a / 20
corner = -a / 2, -a / 2, 0
r = 0.3
corner_in = [-r / 2, -r / 2, 0]


data_dir = "data"

# -------- 2D tri --------

geom = gl.Geometry(2, data_dir=data_dir, model_name="tri")

cell = geom.add_square(corner, a)
squ = geom.add_square(corner_in, r)
incl, cell = cell / squ
geom.add(cell, "cell")
geom.add(incl, "inclusion")
squ_bnds = geom.get_boundaries("inclusion")
geom.add(squ_bnds, "squ_bnds")
geom.set_size("inclusion", pmesh)
geom.set_size("cell", pmesh)
# point = geom.add_point(center)
# geom.add_physical(point.tag, "point1", dim=0)
geom.build()
geom.finalize()  #


ashx.gmsh2dolfinx(
    0,
    mshfile=data_dir + "/tri.msh",
    dim=2,
)

# -------- 2D quads --------

geom = gl.Geometry(2, data_dir=data_dir, model_name="quad", quad=True)


cell = geom.add_square(corner, a)
squ = geom.add_square(corner_in, r)
incl, cell = cell / squ
# geom.add_point(center)
geom.add(cell, "cell")
geom.add(incl, "inclusion")
geom.set_size("inclusion", pmesh)
geom.set_size("cell", pmesh)
# geom.mesh.recombine()

geom.build()
geom.finalize()  #


ashx.gmsh2dolfinx(
    0,
    mshfile=data_dir + "/quad.msh",
    dim=2,
    quad="True",
)


# -------- 3D tets --------

center = (0, 0, 0)

geom = gl.Geometry(3, data_dir=data_dir, model_name="tet")

w = a / 3
cell = geom.add_box(center, (a, a, a))
incl = geom.add_box((a / 2 - w / 2, a / 2 - w / 2, a / 2 - w / 2), (w, w, w))

incl, cell = cell / incl
# point = geom.add_point(center) ####### This makes gmsh2dolfinx fail!!
geom.add(cell, "cell")
geom.add(incl, "inclusion")
bnds = geom.get_boundaries("inclusion")
geom.add(bnds, "inclusion_bnds")
# lines = geom.get_boundaries("inclusion_bnds",dim=2)
# geom.add_physical(lines, "lines", dim=1)
# geom.add_physical(point.tag, "point", dim=0)
geom.set_size("inclusion", pmesh)
geom.set_size("cell", pmesh)
geom.build()

mesh, ct, ft, msh = ashx.gmsh2dolfinx(
    0,
    mshfile=data_dir + "/tet.msh",
    dim=3,
)


# -------- 3D hex --------

geom = gl.Geometry(dim=3, data_dir=data_dir, model_name="hex", quad=True)

w = a / 3
# cell = geom.add_box(center, (a,a,a))
Nx = Ny = Nz = 10
p = geom.add_point((0, 0, 0))
line = geom.extrude([(0, p.tag)], a, 0, 0, [Nx])
s = geom.extrude([line[1]], 0, a, 0, [Ny], recombine=True)
v = geom.extrude([s[1]], 0, 0, a, [Nz], recombine=True)
geom.add_physical(v[1][1], "cell")
bnds = geom.get_boundaries("cell")
geom.add(bnds[0], "bnds")
geom.set_size("cell", pmesh)
geom.build()


mesh, ct, ft, msh = ashx.gmsh2dolfinx(
    0,
    mshfile=data_dir + "/hex.msh",
    dim=3,
    quad=True,
)
