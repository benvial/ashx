#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: benvial
# This file is part of ashx
# License: GPLv3
# See the documentation at  gitlab.com/benvial/ashx


import os

import numpy as np
from dolfinx.fem import Function

import ashx


def test_geo():
    data_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data")
    geom = ashx.BaseGeometry(
        mshfile=os.path.join(data_dir, "tri.msh"),
        dim=2,
    )

    f = Function(geom.Vmat)
    f.x.petsc_vec.array = 1

    vol = geom.integrate(f)
    volc = geom.integrate(geom.constant(1))

    assert np.allclose(vol, 1)
    assert sum(geom.cell_vol) == vol == volc

    vol_incl = geom.get_volume("inclusion")

    assert np.allclose(vol_incl, 0.3**2)

    pwdict2 = {}
    for i, subdomain in enumerate(geom.subdomains):
        pwdict2[subdomain] = i + 1
    fun1 = geom.piecewise(pwdict2)

    pwdict2 = dict(cell=lambda x: x[0] * 2, inclusion=lambda x: np.cos(x[1]))
    geom.piecewise(pwdict2)

    s1 = np.ones(geom.num_sub_cells["cell"])
    s2 = np.ones(geom.num_sub_cells["inclusion"]) * 2
    pwdict3 = dict(cell=s1, inclusion=s2)
    fun3 = geom.piecewise(pwdict3)
    assert np.allclose(fun1.x.array, fun3.x.array)
