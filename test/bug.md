
# Environment




```
mamba activate base
rmenv ashx || echo Skip
mamba env create -f environment.yml
mamba activate ashx
python test/bug_fenicsx.py
```


<!-- pip install gmsh -->
<!-- pip install --force-reinstall fenics-basix  ## This solves the issue -->

```
rmenv fenicsx-env | echo Skip
mamba create -n fenicsx-env
mamba activate fenicsx-env
mamba install -c conda-forge fenics-dolfinx
pip install gmsh
pip install --force-reinstall fenics-basix  ## This solves the issue
python bug_fenicsx.py
```


```
mamba create -n fenicsx-env-py311 "python=3.11"
mamba activate fenicsx-env-py311
mamba install -c conda-forge fenics-dolfinx gmsh python-gmsh
```


```
mamba create -n fenicsx-env-2
mamba activate fenicsx-env-2
mamba install -c conda-forge "fenics-dolfinx<0.8.0" gmsh python-gmsh
```
