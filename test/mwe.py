#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: benvial
# This file is part of ashx
# License: GPLv3
# See the documentation at  gitlab.com/benvial/ashx


import gmsh
from dolfinx.io import gmshio
from mpi4py import MPI

gmsh.initialize()
gmsh.model.add("MWE")
box = gmsh.model.occ.addRectangle(0, 0, 0, 1, 1)
point = gmsh.model.occ.addPoint(0.5, 0.5, 0)
gmsh.model.occ.synchronize()
# gmsh.model.addPhysicalGroup(0, [point], name="point")
gmsh.model.addPhysicalGroup(2, [box], name="box")
gmsh.model.occ.synchronize()
gmsh.model.mesh.generate(2)
gmsh.write("mesh2D.msh")

gmsh.finalize()


mesh, cell_markers, facet_markers = gmshio.read_from_msh(
    "mesh2D.msh", MPI.COMM_WORLD, gdim=2
)

# gmsh.fltk.run()
# gmsh.finalize()
# model = gmsh.model
# x = gmshio.extract_geometry(model)
# topologies = gmshio.extract_topology_and_markers(model)

# xsx
# model_rank = 0

# mesh, cell_tags, facet_tags = gmshio.model_to_mesh(
#     gmsh.model, MPI.COMM_WORLD, model_rank)
# gmsh.finalize()

# # mesh, cell_tags, facet_tags = gmshio.read_from_msh(
# #     "mesh2D.msh", MPI.COMM_WORLD, 0, gdim=2)
