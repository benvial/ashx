import sys

import basix
import dolfinx
import ffcx
import gmsh
import numpy
import ufl
from dolfinx.io import XDMFFile, gmshio
from mpi4py import MPI

print("Package versions")
print("------------------")
print("Python: ", sys.version)
print("dolfinx: ", dolfinx.__version__)
print("basix: ", basix.__version__)
print("ffcx: ", ffcx.__version__)
print("ufl: ", ufl.__version__)
print("gmsh: ", gmsh.__version__)
print("numpy: ", numpy.__version__)


##############################################################################
gmsh.initialize()
# Create model
model = gmsh.model()
sphere = model.occ.addSphere(0, 0, 0, 1, tag=1)
model.occ.synchronize()
model.add_physical_group(dim=3, tags=[sphere])

# Generate the mesh
model.mesh.generate(dim=3)
comm = MPI.COMM_SELF
msh, ct, ft = gmshio.model_to_mesh(model, comm, rank=0)
